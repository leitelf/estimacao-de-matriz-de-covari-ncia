function C = covar_69(X)
  % Eq 69
  % Os vetores aleatorios no material de referencia da eq 69 sao vetores colunas.
  % Devido as disposicoes dos dados na base ionosphere e a formatacao adotada na
  % funcao cov, nativa do Octave, os vetores aleatorios aqui sao esperados como
  % linhas.
  % C = R - m' * m
  
  [N, p] = size(X);
  m = sum(X) / N; % Vetor medio
  R = (X' * X) / N; % Matriz de correlacao
  C = R - m' * m; % Matriz de covariancia
endfunction
