function C = covar_68(X)
  % Eq 68
  % Os vetores aleatorios no material de referencia da eq 68 sao vetores colunas.
  % Devido as disposicoes dos dados na base ionosphere e a formatacao adotada na
  % funcao cov, nativa do Octave, os vetores aleatorios aqui sao esperados como
  % linhas.
  % C = (1/N) * Sum(1, N) {[x(n) - m]' * [x(n) - m]}
  
  [N p] = size(X);
  C = zeros(p);
  m = sum(X) / N; % Vetor medio
  for n = 1:N % Para cada vetor aleatorio
    aux = X(n,:) - m;
    C = C + aux' * aux;
  endfor
  C = C / N; % Matriz de covariancia
endfunction
