function [t1, t2, t3, t4, t5] = bench_covar(X)
  C_REF = cov(X, 1);
  % Implementation from eq 68
  disp("Eq 68 implementation:");
  t1 = [];
  for i=1:1000
    tic();
    C_68 = covar_68(X);
    tmpT = toc(); 
    t1 = [t1; tmpT];
  endfor
  D_68 = C_68 - C_REF;
  D_68 = abs(D_68(:,:));
  D_68V = D_68(:);
  
  dmea1 = mean(D_68V)
  dmed1 = median(D_68V)
  dvar1 = var(D_68V)
  dmax1 = max(D_68V)
  dmin1 = min(D_68V)
  
  mea1 = mean(t1)
  med1 = median(t1)
  var1 = var(t1)
  max1 = max(t1)
  min1 = min(t1)
  disp("\n");
  
  % Implementation from eq 69
  disp("Eq 69 implementation:");
  t2 = [];
  for i=1:1000
    tic();
    C_69 = covar_69(X);
    tmpT = toc();
    t2 = [t2; tmpT];
  endfor
  D_69 = C_69 - C_REF;
  D_69 = abs(D_69(:,:));
  D_69V = D_69(:);
  
  dmea2 = mean(D_69V)
  dmed2 = median(D_69V)
  dvar2 = var(D_69V)
  dmax2 = max(D_69V)
  dmin2 = min(D_69V)
  
  mea2 = mean(t2)
  med2 = median(t2)
  var2 = var(t2)
  max2 = max(t2)
  min2 = min(t2)
  disp("\n");
  
  % Implementation from eq 70
  disp("Eq 70 implementation:");
  t3 = [];
  for i=1:1000
    tic();
    C_70 = covar_70(X);
    tmpT = toc();
    t3 = [t3; tmpT];
  endfor
  D_70 = C_70 - C_REF;
  D_70 = abs(D_70(:,:));
  D_70V = D_70(:);
  
  dmea3 = mean(D_70V)
  dmed3 = median(D_70V)
  dvar3 = var(D_70V)
  dmax3 = max(D_70V)
  dmin3 = min(D_70V)
  
  mea3 = mean(t3)
  med3 = median(t3)
  var3 = var(t3)
  max3 = max(t3)
  min3 = min(t3)
  disp("\n");
  
  % Implementation from eq 73
  disp("Eq 73 implementation:");
  t4 = [];
  for i=1:1000
    tic();
    C_73 = covar_73(X);
    tmpT = toc();
    t4 = [t4; tmpT];
  endfor
  D_73 = C_73 - C_REF;
  D_73 = abs(D_73(:,:));
  D_73V = D_73(:);
  
  dmea4 = mean(D_73V)
  dmed4 = median(D_73V)
  dvar4 = var(D_73V)
  dmax4 = max(D_73V)
  dmin4 = min(D_73V)
  
  mea4 = mean(t4)
  med4 = median(t4)
  var4 = var(t4)
  max4 = max(t4)
  min4 = min(t4)
  disp("\n");
  
  % Implementation from octave
  disp("Octave implementation:");
  t5 = [];
  for i=1:1000
    tic();
    C_OCT = cov(X, 1);
    tmpT = toc();
    t5 = [t5; tmpT];
  endfor
  D_OCT = C_OCT - C_REF;
  D_OCT = abs(D_OCT(:,:));
  D_OCTV = D_OCT(:);
  
  dmea5 = mean(D_OCTV)
  dmed5 = median(D_OCTV)
  dvar5 = var(D_OCTV)
  dmax5 = max(D_OCTV)
  dmin5 = min(D_OCTV)
  
  mea5 = mean(t5)
  med5 = median(t5)
  var5 = var(t5)
  max5 = max(t5)
  min5 = min(t5)
  disp("\n");
  
  disp("Octave implementation:");
  t6 = [];
  for i=1:1000
    tic();
    C_OCT2 = covar_oct(X, 1);
    tmpT = toc();
    t6 = [t6; tmpT];
  endfor
  D_OCT2 = C_OCT2 - C_REF;
  D_OCT2 = abs(D_OCT2(:,:));
  D_OCTV2 = D_OCT2(:);
  
  dmea6 = mean(D_OCTV2)
  dmed6 = median(D_OCTV2)
  dvar6 = var(D_OCTV2)
  dmax6 = max(D_OCTV2)
  dmin6 = min(D_OCTV2)
  
  mea6 = mean(t6)
  med6 = median(t6)
  var6 = var(t6)
  max6 = max(t6)
  min6 = min(t6)
  disp("\n");
  
  
endfunction
