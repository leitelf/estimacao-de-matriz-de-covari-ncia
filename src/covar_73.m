function C = covar_73(X)
  % Eq 73
  % Os vetores aleatorios no material de referencia da eq 73 sao vetores colunas.
  % Devido as disposicoes dos dados na base ionosphere e a formatacao adotada na
  % funcao cov, nativa do Octave, os vetores aleatorios aqui sao esperados como
  % linhas.
  % m(n) = a * m(n-1) + (1-a) * x(n)
  % m(0) = vetor-nulo
  % R(n) = a * R(n-1) + (1-a) * x(n)' * x(n)
  % R(n) = matriz identidade
    
  [N, p] = size(X);
  m = zeros(1, p); % m(0) = vetor-nulo
  R = eye(p); % R(0) = matriz identidade
  for n = 1:N % Para cada vetor aleatorio
    x = X(n, :);
    a = (n - 1)/n;
    m = a * m + (1 - a) * x; % Atualiza vetor medio
    R = a * R + (1 - a) * x' * x; % Atualiza matriz de correlacao
  endfor
  
  C = R - m' * m;
endfunction