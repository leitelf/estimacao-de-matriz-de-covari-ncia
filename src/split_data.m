function [G, B] = split_data(X)
  [N, p] = size(X);
  G = [];
  B = [];
  for i=1:N
    if X(i,p) == 1
      G = vertcat(G, X(i,:));
    else
      B = vertcat(B, X(i,:));
    endif
  endfor
endfunction
