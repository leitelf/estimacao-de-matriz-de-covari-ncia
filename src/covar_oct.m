function C = covar_oct(X)
  % Eq 70
  % Os vetores aleatorios no material de referencia da eq 70 sao vetores colunas.
  % Devido as disposicoes dos dados na base ionosphere e a formatacao adotada na
  % funcao cov, nativa do Octave, os vetores aleatorios aqui sao esperados como
  % linhas.
  % C = (1/N) * [X - M]' * [X - M]
  
  [N, p] = size(X);
  X = center(X, 1);
  C = X' * X / (N);
  
endfunction
